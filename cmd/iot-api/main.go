package main

import (
	"context"
	"fmt"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd"
	"gitee.com/thingple/iot-api/internal/db"
	"gitee.com/thingple/iot-api/internal/etc"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/persistence"
	"github.com/lishimeng/app-starter/token"
	"github.com/lishimeng/go-log"
	"github.com/lishimeng/x/container"
	"time"
)

import _ "github.com/lib/pq"

func main() {

	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()

	err := _main()
	if err != nil {
		fmt.Println(err)
	}
	time.Sleep(time.Second * 2)
}

func _main() (err error) {
	application := app.New()

	err = application.Start(func(ctx context.Context, builder *app.ApplicationBuilder) error {

		var err error
		err = builder.LoadConfig(&etc.Config, nil)
		if err != nil {
			return err
		}

		dbConfig := persistence.PostgresConfig{
			UserName:  etc.Config.Db.User,
			Password:  etc.Config.Db.Password,
			Host:      etc.Config.Db.Host,
			Port:      etc.Config.Db.Port,
			DbName:    etc.Config.Db.Database,
			InitDb:    true,
			AliasName: "default",
			SSL:       etc.Config.Db.Ssl,
		}

		if etc.Config.Token.Enable {
			issuer := etc.Config.Token.Issuer
			tokenKey := []byte(etc.Config.Token.Key)
			builder = builder.EnableTokenValidator(func(inject app.TokenValidatorInjectFunc) {
				provider := token.NewJwtProvider(token.WithIssuer(issuer),
					token.WithKey(tokenKey, tokenKey), // hs256的秘钥必须是[]byte
					token.WithAlg("HS256"),
				)
				storage := token.NewLocalStorage(provider)
				container.Add(provider)
				inject(storage)
			})
		}

		builder.
			EnableDatabase(dbConfig.Build(), db.RegisterTable()...).
			SetWebLogLevel("debug").
			//EnableAmqp(amqp.Connector{Conn: etc.Config.Mq.MQConn}).
			EnableWeb(etc.Config.Web.Listen, ddd.Route).
			PrintVersion()
		return err
	}, func(s string) {
		log.Info(s)
	})

	return
}
