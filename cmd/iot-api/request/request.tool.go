package request

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-infrastructure/common"
	"gitee.com/thingple/iot-open/oss"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/go-log"
)

var (
	tenantMap = make(map[string]int)
)

// GetOrgCode 查询租户信息真实性
func GetOrgCode(ctx server.Context) (tenantCode string, tenantId int) {
	tenantCode = ctx.C.GetHeader(oss.HeaderTenant)
	if id, ok := tenantMap[tenantCode]; ok {
		tenantId = id
		return
	}
	// 查询租户信息真实性
	var tenant model.Organization
	err := app.GetOrm().Context.
		QueryTable(new(model.Organization)).
		Filter("Code", tenantCode).
		One(&tenant)
	if err != nil {
		log.Debug(err)
		return
	}
	tenantMap[tenantCode] = tenant.Id
	tenantId = tenant.Id
	return
}

func GetPageParams(ctx server.Context) (page common.SimplePageRequest) {
	page.From = ctx.C.URLParamIntDefault("from", 0)
	page.Size = ctx.C.URLParamIntDefault("size", 10)
	return
}
