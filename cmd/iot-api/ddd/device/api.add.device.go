package device

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

type ReqAdd struct {
	DeviceName       string `json:"deviceName,omitempty"`
	DeviceCode       string `json:"deviceCode,omitempty"`
	AppId            string `json:"appId,omitempty"`
	DeviceCategoryId int    `json:"deviceCategoryId,omitempty"`
	DeviceProfileId  int    `json:"deviceProfileId,omitempty"`
	OrgId            int    `json:"orgId,omitempty"`

	LastReport      int64  `json:"lastReport"`
	Timeout         int    `json:"timeout"`
	NetworkConfigId string `json:"networkConfigId"`
}

type RespAdd struct {
	DeviceId int `json:"deviceId,omitempty"`
	app.Response
}

func AddDevice(ctx server.Context) {
	var err error
	var req ReqAdd
	var resp RespAdd

	// &req
	err = ctx.C.ReadJSON(&req)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	// DeviceCode is required
	if len(req.DeviceCode) == 0 {
		log.Info("addDevice err")
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	d := model.Device{DeviceCode: req.DeviceCode}
	// verify DeviceID
	err = app.GetOrm().Context.Read(&d, "device_code")
	if err != nil {
		log.Info("addDevice err", err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	if d.DeviceID > 0 {
		log.Info("DeviceID already exist")
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	// New
	nd := model.Device{
		DeviceName:      req.DeviceName,
		DeviceCode:      req.DeviceCode,
		AppId:           req.AppId,
		LastReport:      req.LastReport,
		Timeout:         req.Timeout,
		DeviceProfileId: req.DeviceProfileId,
		NetworkConfigId: req.NetworkConfigId,
		OrgId:           req.OrgId,
	}
	// add
	_, err = app.GetOrm().Context.Insert(&nd)
	if err != nil {
		log.Info("Add Device err", err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}

	resp.DeviceId = nd.DeviceID
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
