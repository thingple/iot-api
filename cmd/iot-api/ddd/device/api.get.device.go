package device

import (
	"gitee.com/thingple/iot-api/cmd/iot-api/request"
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetDeviceApi(ctx server.Context) {
	var err error
	var resp response.DeviceResponse
	orgCode, orgId := request.GetOrgCode(ctx)
	if orgId <= 0 {
		log.Info("unknown org %s", orgCode)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}

	deviceCode := ctx.C.Params().GetString("deviceCode")
	log.Info("[GetDeviceApi] deviceCode=%s", deviceCode)
	var device model.Device
	device.DeviceCode = deviceCode
	device.OrgId = orgId
	err = app.GetOrm().Context.Read(&device, "DeviceCode", "OrgId")
	if err != nil {
		log.Info("device fail %s", err)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	var application model.Application
	application.AppId = device.AppId
	err = app.GetOrm().Context.Read(&application, "AppId")
	if err == nil {
		resp.DeviceID = device.DeviceID
		resp.DeviceName = device.DeviceName
		resp.DeviceCode = device.DeviceCode
		resp.AppId = device.AppId
	}
	resp.SkuCode = device.SkuCode
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
