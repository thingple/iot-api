package ossDevice

import (
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/go-log"
)

type reqDeleteOssDevice struct {
	DeviceCode string `json:"deviceCode"`
}

func DeleteOssDevice(ctx server.Context) {
	var req reqDeleteOssDevice
	var resp response.OssDeviceResponse
	deviceCode := ctx.C.Params().GetString("deviceCode")
	resp.DeviceCode = deviceCode
	log.Info("req", req)
	log.Info("resp", resp)
	ctx.Json(resp)
}
