package ossDevice

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-infrastructure/network/nwconnector"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func AddOssDevice(ctx server.Context) {
	var err error
	var resp response.OssDeviceResponse
	deviceCode := ctx.C.Params().GetString("deviceCode")
	device := model.Device{DeviceCode: deviceCode}
	err = app.GetOrm().Context.Read(&device, "device_code")
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	item := model.Items{ItemCode: device.DeviceCode}
	err = app.GetOrm().Context.Read(&item, "item_code")
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	var skuConfigureItemsList []model.SkuConfigureItems
	_, err = app.GetOrm().Context.QueryTable(new(model.SkuConfigureItems)).Filter("items_id", item.Id).All(&skuConfigureItemsList)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	networkConfig := model.NetworkConfig{NetworkId: device.NetworkConfigId}
	err = app.GetOrm().Context.Read(&networkConfig, "network_id")
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	//解码
	data, err := decodeConfig(networkConfig.Config, networkConfig.NetworkType)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	resp.DeviceCode = device.DeviceCode
	resp.Data = data
	log.Info("resp:", resp)
	//TODO 更新item->ossStatus int？string？
	ossStatus := model.OssDeviceStatusNormal
	item.OssStatus = int(ossStatus)
	_, err = app.GetOrm().Context.Update(&item)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		ctx.Json(resp)
		return
	}
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
func decodeConfig(config, configType string) (data interface{}, err error) {
	bs, err := base64.StdEncoding.DecodeString(config)
	if err != nil {
		return
	}
	var c interface{}
	switch nwconnector.NetworkType(configType) {
	case nwconnector.Mqtt:
		c = new(nwconnector.MqttConfig)
	case nwconnector.Lorawan:
		c = new(nwconnector.LorawanMqttConfig)
	case nwconnector.Tree:
		c = new(nwconnector.MqttConfig)
	default:
		err = errors.New("type err")
		return
	}
	data, err = configUnmarshal(bs, c)
	return
}
func configUnmarshal(bs []byte, c interface{}) (data interface{}, err error) {
	if len(bs) == 0 {
		data = c
		return
	}
	err = json.Unmarshal(bs, &c)
	data = c
	return
}
