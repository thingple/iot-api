package ble

import (
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

type Resp struct {
	Code interface{} `json:"code,omitempty"`
	Cmd  string      `json:"cmd,omitempty"`
}

func GetCodeSourceApi(ctx server.Context) {
	code := ctx.C.Params().GetString("code")
	log.Info("[GetCodeSourceApi] code=%s", code)

	var resp Resp
	resp.Code = tool.RespCodeSuccess
	resp.Cmd = "RXMQTT=emq.thingplecloud.com:1883,lora_app_server,ithingpleo"
	ctx.Json(resp)
}
