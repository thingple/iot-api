package profile

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetProfileApi(ctx server.Context) {
	var resp response.ProfileResponse
	var err error
	deviceProfileId := ctx.C.Params().GetIntDefault("deviceProfileId", 0)
	log.Info("[GetProfileApi] deviceProfileId=%d", deviceProfileId)
	if deviceProfileId == 0 {
		log.Info("deviceProfileId is null %d", deviceProfileId)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	var profile model.DeviceProfile
	profile.Id = deviceProfileId
	err = app.GetOrm().Context.Read(&profile, "Id")
	if err != nil {
		log.Info("profile fail %s", err)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	var deviceCodec model.DeviceCodec
	deviceCodec.DeviceProfileId = deviceProfileId
	err = app.GetOrm().Context.Read(&deviceCodec, "DeviceProfileId")
	if err == nil {
		resp.DeviceProfileId = deviceCodec.DeviceProfileId
		resp.DecodeHandler = deviceCodec.DecodeHandler
		resp.EncodeHandler = deviceCodec.EncodeHandler
		resp.Id = deviceCodec.Id
	}
	deviceDataPoints := make([]response.DataPoint, 0)
	qs := app.GetOrm().Context.QueryTable(new(model.DeviceDataPoint)).Filter("DeviceProfileId", deviceProfileId)
	qs.All(&deviceDataPoints)
	if len(deviceDataPoints) > 0 {
		for _, item := range deviceDataPoints {
			var d = response.DataPoint{
				Id:              item.Id,
				DeviceProfileId: item.DeviceProfileId,
				Index:           item.Index,
				Name:            item.Name,
				Type:            item.Type,
				MinLength:       item.MinLength,
				MaxLength:       item.MaxLength,
				EnableLink:      item.EnableLink,
				Default:         item.Default,
			}
			deviceDataPoints = append(deviceDataPoints, d)
		}
	}
	resp.DataPoints = deviceDataPoints
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
