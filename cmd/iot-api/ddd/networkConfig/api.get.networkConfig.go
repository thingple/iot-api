package networkConfig

import (
	"gitee.com/thingple/iot-api/cmd/iot-api/request"
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-infrastructure/common"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetNetworkConfigListApi(ctx server.Context) {
	var resp response.NetworkConfigPageResponse
	orgCode, orgId := request.GetOrgCode(ctx)
	if orgId <= 0 {
		log.Info("unknown org %s", orgCode)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	pageReq := request.GetPageParams(ctx)
	configs, err := selectNetConfigs(orgId, pageReq)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		resp.Message = "查询失败"
		ctx.Json(resp)
		return
	}
	if len(configs) > 0 {
		for _, item := range configs {
			configItem := response.NetworkConfig{
				Id:          item.Id,
				OrgId:       orgCode,
				NetworkId:   item.NetworkId,
				NetworkName: item.NetworkName,
				NetworkType: item.NetworkType,
				Config:      item.Config,
				Status:      item.Status,
			}
			resp.Items = append(resp.Items, configItem)
		}
	}
	resp.Code = tool.RespCodeSuccess
	resp.Message = "查询成功"
	ctx.Json(resp)
}
func selectNetConfigs(org int, page common.SimplePageRequest) (log []model.NetworkConfig, err error) {
	//result_set
	_, err = app.GetOrm().Context.
		QueryTable(new(model.NetworkConfig)).
		Filter("org", org).
		Filter("id__gt", page.From).
		Limit(page.Size).
		OrderBy("id").All(&log)
	if err != nil {
		return
	}

	return
}
