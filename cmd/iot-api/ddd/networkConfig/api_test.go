package networkConfig

import (
	"gitee.com/thingple/iot-open/oss"
	"github.com/lishimeng/go-log"
	"testing"
)

func TestNetworkApi(t *testing.T) {
	log.SetLevelAll(log.DEBUG)
	client, err := oss.New("http://localhost:80/api", oss.WithTenant("efsfbgngfnghnhgghm"))
	if err != nil {
		t.Fatal(err)
		return
	}

	application, err := client.NetworkConfigs()
	if err != nil {
		t.Fatal(err)
		return
	}
	t.Logf("res: %+v", application)
	t.Log("success")
}
