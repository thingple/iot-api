package application

import (
	"fmt"
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetApplicationApi(ctx server.Context) {
	var resp response.ApplicationResponse
	appId := ctx.C.Params().GetString("appId")
	log.Info("[GetApplicationApi] appId = %s", appId)
	var err error
	var application model.Application
	application.AppId = appId
	err = app.GetOrm().Context.Read(&application, "AppId")
	if err != nil {
		log.Info("application fail ,appId=%s", appId)
		log.Info(err)
		resp.Response.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	hook := model.ApplicationHookConfig{AppId: appId}
	err = app.GetOrm().Context.Read(&hook, "AppId")
	if err != nil {
		log.Info("ApplicationHookConfig fail ,appId=%s", appId)
		log.Info(err)
	}
	resp.Application = response.Application{
		Id:              application.Id,
		Name:            application.Name,
		AppId:           application.AppId,
		Description:     application.Description,
		CodecType:       application.CodecType,
		DeviceProfileId: application.DeviceProfileId,
		Status:          application.Status,
		HookConfigId:    hook.Id,
	}
	resp.Application.Org = fmt.Sprintf("%d", application.OrgId)
	resp.Response.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
