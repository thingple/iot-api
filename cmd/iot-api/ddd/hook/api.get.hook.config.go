package hook

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetHookConfigApi(ctx server.Context) {
	var resp response.HookResponse
	var err error
	id := ctx.C.Params().GetIntDefault("id", 0)
	log.Info("[GetHookConfigApi]id=%d", id)
	var applicationHookConfig model.ApplicationHookConfig
	applicationHookConfig.Id = id
	err = app.GetOrm().Context.Read(&applicationHookConfig)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}

	resp.Code = tool.RespCodeSuccess
	resp.Id = applicationHookConfig.Id
	resp.AppId = applicationHookConfig.AppId
	resp.HookType = applicationHookConfig.HookType
	resp.HookHost = applicationHookConfig.HookHost
	resp.HookUser = applicationHookConfig.HookUser
	resp.HookPass = applicationHookConfig.HookPass
	resp.HookTopic = applicationHookConfig.HookTopic
	resp.Status = applicationHookConfig.Status
	ctx.Json(resp)
}
