package ddd

import (
	"fmt"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/application"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/beacon"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/ble"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/codec"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/device"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/hook"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/networkConfig"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/ossDevice"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/profile"
	"gitee.com/thingple/iot-api/cmd/iot-api/ddd/skuCodec"
	"gitee.com/thingple/iot-api/internal/utils"
	"gitee.com/thingple/iot-open/open"
	"gitee.com/thingple/iot-open/oss/resource"
	"github.com/lishimeng/app-starter/server"
)

func Route(app server.Router) {
	root := app.Path("/api")
	innerAPI(root)
	openAPI(root)
	return
}

// innerAPI /api/{Source}/{Id}
func innerAPI(root server.Router) {
	var r resource.Resource
	r = resource.GetResource(resource.Device)
	root.Get(r.One("{deviceCode:string}"), device.GetDeviceApi)
	root.Post(r.Add(), device.AddDevice)

	r = resource.GetResource(resource.Application)
	root.Get(r.One("{appId:string}"), application.GetApplicationApi)

	r = resource.GetResource(resource.Profile)
	root.Get(r.One("{deviceProfileId:int}"), profile.GetProfileApi)

	r = resource.GetResource(resource.SkuCodec)
	root.Get(r.One("{skuId:int}"), skuCodec.GetSkuCodecApi)

	r = resource.GetResource(resource.Hook)
	root.Get(r.One("{id:int}"), hook.GetHookConfigApi)

	root.Get("/code/{code}", ble.GetCodeSourceApi)

	r = resource.GetResource(resource.NetworkConfig)
	root.Get(r.List(), networkConfig.GetNetworkConfigListApi)

	r = resource.GetResource(resource.Codec)
	root.Get(r.List(), codec.GetCodec)
	root.Get(r.One("{id:int}"), codec.Get)
	//root.Post(r.Add(), codec.AddCodec)
	r = resource.GetResource(resource.OssDevice)
	root.Get(r.One("{deviceCode:string}"), ossDevice.AddOssDevice)
	root.Delete(r.Delete("{deviceCode:string}"), ossDevice.DeleteOssDevice)
}

// openAPI "/api/{ApiVersion}/{Source}/{Id}"  Header:{"Authorization":"Bearer Token"}
func openAPI(root server.Router) {
	root.Get(fmt.Sprintf(open.PathBeaconInfo, "{deviceCode:string}"), utils.WithAuth(beacon.GetBeaconInfoApi)...)
	root.Get(fmt.Sprintf(open.PathBeaconState, "{deviceCode:string}"), utils.WithAuth(beacon.GetBeaconStateApi)...)
}
