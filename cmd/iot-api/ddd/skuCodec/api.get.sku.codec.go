package skuCodec

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetSkuCodecApi(ctx server.Context) {
	var resp response.SkuCodecResponse
	var err error
	skuId := ctx.C.Params().GetDefault("skuId", 0)
	if skuId == 0 {
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	var skuCodec model.SkuCodec
	skuCodec.SkuId = skuId.(int)
	err = app.GetOrm().Context.Read(&skuCodec, "SkuId")
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	resp.SkuId = skuCodec.SkuId
	resp.Channels = skuCodec.Channels
	resp.DecodeHandler = skuCodec.DecodeHandler
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
