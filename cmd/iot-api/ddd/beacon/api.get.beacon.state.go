package beacon

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/open"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetBeaconStateApi(ctx server.Context) {
	var err error
	var resp open.BeaconStateResponse
	deviceCode := ctx.C.Params().GetString("deviceCode")
	log.Info("[GetBeaconStateApi] deviceCode=%s", deviceCode)

	var beaconState model.ItemStatus
	beaconState.ItemCode = deviceCode
	err = app.GetOrm().Context.Read(&beaconState, "ItemCode")
	if err != nil {
		log.Info("[GetBeaconStateApi] deviceCode=%s, Read err: %s", deviceCode, err.Error())
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}

	resp.DeviceCode = beaconState.ItemCode
	resp.Latitude = beaconState.LastLatitude
	resp.Longitude = beaconState.LastLongitude
	resp.Gateway = beaconState.Memo
	resp.RxTime = beaconState.LastUpdateTime

	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
