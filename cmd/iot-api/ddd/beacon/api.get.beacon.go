package beacon

import (
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-open/open"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

func GetBeaconInfoApi(ctx server.Context) {
	var err error
	var resp open.BeaconInfoResponse
	deviceCode := ctx.C.Params().GetString("deviceCode")
	log.Info("[GetBeaconInfoApi] deviceCode=%s", deviceCode)

	var beacon model.ItemLocation
	beacon.ItemCode = deviceCode
	err = app.GetOrm().Context.Read(&beacon, "ItemCode")
	if err != nil {
		log.Info("[GetBeaconInfoApi]Read deviceCode=%s, err: %s", deviceCode, err.Error())
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	resp.DeviceCode = beacon.ItemCode
	resp.DeviceSN = beacon.ItemSN
	resp.DeviceName = beacon.Description
	resp.Channel = beacon.ChannelNo
	resp.IsActive = beacon.IsActive

	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
