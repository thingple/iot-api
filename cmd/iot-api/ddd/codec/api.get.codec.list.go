package codec

import (
	"gitee.com/thingple/iot-api/cmd/iot-api/request"
	"gitee.com/thingple/iot-api/internal/db/model"
	"gitee.com/thingple/iot-infrastructure/common"
	"gitee.com/thingple/iot-open/oss/response"
	"github.com/beego/beego/v2/client/orm"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/app-starter/server"
	"github.com/lishimeng/app-starter/tool"
	"github.com/lishimeng/go-log"
)

// GetList 取列表，可以用owner+category联合筛选(可选), 这个是给外部用的，组织ID查询(必须)
// page参数限制返回的数据量 open.PageRequest 必要时使用默认的page参数
func GetList(ctx server.Context) {
	var resp response.CodecPageResponse
	owner := ctx.C.URLParamDefault("owner", "")
	category := ctx.C.URLParamIntDefault("category", 0)
	orgCode, orgId := request.GetOrgCode(ctx)
	if orgId <= 0 {
		log.Info("unknown org %s", orgCode)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	pageReq := request.GetPageParams(ctx)
	codecList, err := selectCodecList(owner, category, orgId, pageReq)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		resp.Message = "查询失败"
		ctx.Json(resp)
		return
	}
	if len(codecList) > 0 {
		for _, item := range codecList {
			codec := response.Codec{
				Id:            item.Id,
				Channels:      item.Channels,
				Owner:         item.Owner,
				Category:      response.CodecCategory(item.Category),
				DecodeHandler: item.DecodeHandler,
				Describe:      item.Describe,
			}
			resp.Items = append(resp.Items, codec)
		}
		resp.Code = tool.RespCodeSuccess
		resp.Message = "查询成功"
	} else {
		resp.Code = tool.RespCodeNotFound
		resp.Message = "not found"
	}

	ctx.Json(resp)
}

// GetCodec 取列表，owner+category联合筛选, 这个是给外部用的，没查询条件不查询
func GetCodec(ctx server.Context) {
	var resp response.CodecPageResponse
	owner := ctx.C.URLParamDefault("owner", "")
	category := ctx.C.URLParamIntDefault("category", 0)

	pageReq := request.GetPageParams(ctx)
	codecList, err := selectCodecList(owner, category, 0, pageReq)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeError
		resp.Message = "查询失败"
		ctx.Json(resp)
		return
	}
	if len(codecList) > 0 {
		for _, item := range codecList {
			codec := response.Codec{
				Id:            item.Id,
				Channels:      item.Channels,
				Owner:         item.Owner,
				Category:      response.CodecCategory(item.Category),
				DecodeHandler: item.DecodeHandler,
				Describe:      item.Describe,
			}
			resp.Items = append(resp.Items, codec)
		}
		resp.Code = tool.RespCodeSuccess
		resp.Message = "查询成功"
	} else {
		resp.Code = tool.RespCodeNotFound
		resp.Message = "not found"
	}

	ctx.Json(resp)
}

// Get 使用ID查询
func Get(ctx server.Context) {
	var resp response.Codec
	id := ctx.C.Params().GetIntDefault("id", 0)
	if id == 0 {
		log.Info("id == 0")
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
	}
	codec := model.Codec{
		Pk: app.Pk{Id: id},
	}
	err := app.GetOrm().Context.Read(&codec)
	if err != nil {
		log.Info(err)
		resp.Code = tool.RespCodeNotFound
		ctx.Json(resp)
		return
	}
	resp = response.Codec{
		Id:            codec.Id,
		Channels:      codec.Channels,
		Owner:         codec.Owner,
		Category:      response.CodecCategory(codec.Category),
		DecodeHandler: codec.DecodeHandler,
		Describe:      codec.Describe,
	}
	resp.Code = tool.RespCodeSuccess
	ctx.Json(resp)
}
func selectCodecList(owner string, category, org int, page common.SimplePageRequest) (log []model.Codec, err error) {
	cond := orm.NewCondition()
	if len(owner) > 0 {
		cond = cond.And("owner", owner)
	}
	if category > 0 {
		cond = cond.And("category", category)
	}
	if org > 0 {
		cond = cond.And("org", org)
	}
	cond = cond.And("id__gt", page.From)
	//result_set
	_, err = app.GetOrm().Context.
		QueryTable(new(model.Codec)).
		SetCond(cond).
		Limit(page.Size).
		OrderBy("id").All(&log)
	if err != nil {
		return
	}

	return
}
