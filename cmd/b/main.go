package main

import (
	"fmt"
	"github.com/lishimeng/app-starter/buildscript"
)

func main() {
	err := buildscript.Generate("thingple",
		buildscript.Application{
			Name:    "iot-api",
			AppPath: "cmd/iot-api",
			HasUI:   false,
		})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("ok")
	}
}
