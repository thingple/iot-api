package db

import "gitee.com/thingple/iot-api/internal/db/model"

func RegisterTable() (tableList []interface{}) {
	tableList = append(tableList,
		new(model.Application),
		new(model.DeviceCodec),
		new(model.Device),
		new(model.Items),
		new(model.SkuCodec),
		new(model.DeviceDataPoint),
		new(model.DeviceProfile),
		new(model.ApplicationHookConfig),
		new(model.ItemLocation),
		new(model.ItemStatus),
		new(model.NetworkConfig),
		new(model.Organization),
		new(model.Codec),
		new(model.SkuConfigureItems),
	)
	return
}
