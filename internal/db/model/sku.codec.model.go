package model

import "github.com/lishimeng/app-starter"

// SkuCodec SKU解码配置
type SkuCodec struct {
	SkuId         int    `orm:"pk;column(sku_id)" json:"skuId"`
	Channels      int    `orm:"column(channels);null" json:"channels"`            // 通道数
	DecodeHandler string `orm:"column(decode_handler);null" json:"decodeHandler"` // 解码器。 javascript function
	app.TableInfo
}
