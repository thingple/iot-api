package model

import "github.com/lishimeng/app-starter"

type SkuConfigureItems struct {
	app.Pk
	SkuId       int    `orm:"column(sku_id)"`
	ItemsId     int    `orm:"column(items_id)"`                  // 主键。物料ID
	ConfigureId int    `orm:"column(configure_id)"`              // 主键。物料ID
	Value       string `orm:"column(value);default('')"`         // 物料名称
	Describe    string `orm:"column(describe);default('');null"` // 描述
	app.TableChangeInfo
}
