package model

import "github.com/lishimeng/app-starter"

type Device struct {
	DeviceID        int    `orm:"pk;auto;column(device_id)"`
	DeviceName      string `orm:"column(device_name);null"`
	DeviceCode      string `orm:"column(device_code);null"`
	AppId           string `orm:"column(app_id);null"`
	LastReport      int64  `orm:"column(last_report)"`
	Timeout         int    `orm:"column(time_out)"`
	DeviceProfileId int    `orm:"column(device_profile_id);null"`
	NetworkConfigId string `orm:"column(network_config_id);null"`
	OrgId           int    `orm:"column(org_id);null"`
	SkuCode         string `orm:"column(sku_code);null"`
	app.TableInfo
}
