package model

import "github.com/lishimeng/app-starter"

// ItemStatus 物品状态表
type ItemStatus struct {
	app.Pk
	ItemId         int     `orm:"column(item_id);unique" json:"itemId,omitempty"`           // 物品ID
	ItemSN         string  `orm:"column(item_sn);null" json:"itemSN,omitempty"`             // 设备序列号
	ItemCode       string  `orm:"column(item_code);null" json:"itemCode,omitempty"`         // 通信编号
	LastUpdateTime int64   `orm:"column(last_update_time)" json:"lastUpdateTime,omitempty"` // 最后一次上报时间
	LastLongitude  float64 `orm:"column(last_longitude);null" json:"lastLongitude"`         // 最后一次上报纬度
	LastLatitude   float64 `orm:"column(last_latitude);null" json:"LastLatitude"`           // 最近一次上报经度
	LastMessage    string  `orm:"column(last_message)" json:"lastMessage"`                  // 最近一次上报数据
	Memo           string  `orm:"column(memo);null" json:"memo,omitempty"`                  // 关联设备信息
	app.TableChangeInfo
}
