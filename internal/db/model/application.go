package model

import "github.com/lishimeng/app-starter"

// Application app
type Application struct {
	app.Pk
	Name            string `orm:"column(name);null"`
	AppId           string `orm:"column(app_id);unique;null"`
	Description     string `orm:"column(description);null"`
	CodecType       string `orm:"column(codec_type);null"`
	DeviceProfileId int    `orm:"column(device_profile_id);null"`
	OrgId           int    `orm:"column(org_id);null"`
	app.TableChangeInfo
}
