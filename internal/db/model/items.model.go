package model

import "github.com/lishimeng/app-starter"

// Items 物品表
type Items struct {
	app.Pk
	SpuId           int    `orm:"column(spu_id)" json:"spuId,omitempty"`                                      // 产品ID
	SkuId           int    `orm:"column(sku_id)" json:"skuId,omitempty"`                                      // 物料ID
	SN              string `orm:"column(sn);default('')" json:"sn,omitempty"`                                 // 设备本身唯一编号
	ItemCode        string `orm:"column(item_code);unique" json:"itemCode,omitempty"`                         // 唯一物品序列号， 如，02+Be01+000+20220801+000001 = 02Be0100020220801000001；产线编号+产品序列号+供应商编号+入库日期+物品编号
	Category        int    `orm:"column(category);default(10)" json:"category,omitempty"`                     // 10。真实设备 20.虚拟设备
	ParentItemId    int    `orm:"column(parent_item_id)" json:"parentItemId,omitempty"`                       //虚拟设备关联的真实设备的id
	Description     string `orm:"column(description)" json:"description,omitempty"`                           //设备描述
	ProductionDate  string `orm:"column(production_date);default('');null" json:"productionDate,omitempty"`   // 生产日期，格式“20220101”
	ValidityDate    string `orm:"column(validity_date);default('');null" json:"validityDate,omitempty"`       // 有效日期，格式“20220101”
	WarehousingDate string `orm:"column(warehousing_date);default('');null" json:"warehousingDate,omitempty"` // 入库日期，格式“20220101”
	StartDate       string `orm:"column(start_time);default('');null" json:"startDate,omitempty"`             // 开始使用日期，格式“20220101”
	OssStatus       int    `orm:"column(oss_status);" json:"ossStatus,omitempty"`
	app.TableChangeInfo
}

// DictItemStatus 设备状态
type DictItemStatus int

const (
	ItemStatusInit     DictItemStatus = 10 // 初始化入库
	ItemStatusInactive DictItemStatus = 20 // 已出库(未启用)
	ItemStatusActive   DictItemStatus = 30 // 已激活启用
	ItemStatusDisabled DictItemStatus = 30 // 已报废
)

// oss状态
type OssDeviceStatus int

const (
	OssDeviceStatusNormal  OssDeviceStatus = 10 //正常
	OssDeviceStatusExist   OssDeviceStatus = 20 //已经存在
	OssDeviceStatusIllegal OssDeviceStatus = 30 //非法
)
