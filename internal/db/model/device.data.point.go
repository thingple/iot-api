package model

import "github.com/lishimeng/app-starter"

type DeviceDataPoint struct {
	app.Pk
	DeviceProfileId int    `orm:"column(device_profile_id);"`
	Index           int    `orm:"column(index);null;"`
	Name            string `orm:"column(name);null;"`
	Type            int    `orm:"column(type);null;"`
	MinLength       int    `orm:"column(min_length);null;"`
	MaxLength       int    `orm:"column(max_length);null;"`
	EnableLink      int    `orm:"column(enable_liink);null;"`
	Default         string `orm:"column(default);null;"`
	app.TableInfo
}

const (
	UplinkEnable = iota
	DownLinkEnable
	AllLinkEnable
)
