package model

import "github.com/lishimeng/app-starter"

type DeviceCodec struct {
	app.Pk
	DeviceProfileId int    `orm:"column(device_profile_id);unique;null"` // profile
	EncodeHandler   string `orm:"column(encode_handler);null"`           // 编码器
	DecodeHandler   string `orm:"column(decode_handler);null"`           // 解码器
	app.TableChangeInfo
}
