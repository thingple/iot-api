package model

import "github.com/lishimeng/app-starter"

// ItemLocation 物品库位表
type ItemLocation struct {
	app.Pk
	ItemId       int    `orm:"column(item_id);" json:"itemId,omitempty"`               // 设备ID
	ItemCode     string `orm:"column(item_code);" json:"itemCode,omitempty"`           // 通信编号
	ItemSN       string `orm:"column(item_sn);" json:"itemSN,omitempty"`               // 序列号
	SpuId        int    `orm:"column(spu_id);" json:"spuId,omitempty"`                 // 产品ID
	SkuId        int    `orm:"column(sku_id);" json:"skuId,omitempty"`                 // 物料ID
	Category     int    `orm:"column(category);default(10)" json:"category,omitempty"` // 10。真实设备 20.虚拟设备
	ParentItemId int    `orm:"column(parent_item_id)" json:"parentItemId,omitempty"`   //虚拟设备关联的真实设备的id
	Description  string `orm:"column(description);default()" json:"description"`     //设备描述
	IsActive     bool   `orm:"column(is_active)" json:"isActive"`                      //激活状态
	ChannelNo    int    `orm:"column(channel_no)" json:"channelNo"`                    //虚拟通信编号
	OrgId        int    `orm:"column(org_id);"`                                        // 组织ID
	app.TableChangeInfo
}

const (
	ItemCategoryParent = 10
	ItemCategoryChild  = 20
)
