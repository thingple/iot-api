package model

import "github.com/lishimeng/app-starter"

// DeviceProfile 设备配置
type DeviceProfile struct {
	app.Pk
	ProfileName string `orm:"column(profile_name);unique;null"` // 配置文件名称
	Description string `orm:"column(desc);null"`                // 配置文件描述
	VendorId    int    `orm:"column(vendor_id);null"`           // 供应商
	ProductId   string `orm:"column(product_id);null"`          // 型号
	Protocol    string `orm:"column(protocol);null"`            // 通信协议
}
