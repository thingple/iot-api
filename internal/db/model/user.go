package model

import "github.com/lishimeng/app-starter"

type User struct {
	app.Pk
	Name string `orm:"column(name);null"`
	app.TableChangeInfo
}
