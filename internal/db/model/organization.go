package model

import "github.com/lishimeng/app-starter"

// Organization organization
type Organization struct {
	app.Pk
	Name string `orm:"column(name);null"`
	Code string `orm:"column(code);unique;null"`
}
