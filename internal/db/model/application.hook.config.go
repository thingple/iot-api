package model

import "github.com/lishimeng/app-starter"

type ApplicationHookConfig struct {
	app.Pk
	AppId     string `orm:"column(app_id);unique"`
	HookType  string `orm:"column(hook_type);null"`
	HookHost  string `orm:"column(hook_host);null"`
	HookUser  string `orm:"column(hook_user);null"`
	HookPass  string `orm:"column(hook_pass);null"`
	HookTopic string `orm:"column(hook_topic);null"`
	app.TableChangeInfo
}
