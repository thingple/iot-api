package model

import "github.com/lishimeng/app-starter"

// PublicResource 公共资源定义, 0:私有资源, 1:公共资源
type PublicResource int

const (
	IsPublic  PublicResource = 1
	IsPrivate PublicResource = 0 // default
)

type NetworkConfig struct {
	app.TenantPk
	NetworkId   string         `orm:"unique;column(network_id);null"`
	NetworkName string         `orm:"column(network_name);null"`
	NetworkType string         `orm:"column(network_type);null"`
	Host        string         `orm:"column(host);null"`      // Deprecate
	Port        int            `orm:"column(port);null"`      // Deprecate
	Username    string         `orm:"column(username);null"`  // Deprecate
	Password    string         `orm:"column(password);null"`  // Deprecate
	ClientId    string         `orm:"column(client_id);null"` // Deprecate
	Ssl         bool           `orm:"column(ssl);null"`       // Deprecate
	Config      string         `toml:"column(config);null"`
	IsPublic    PublicResource `toml:"column(is_public);null"` // 公共网络/私有网络
	app.TableChangeInfo
}
