package model

import "github.com/lishimeng/app-starter"

type Codec struct {
	app.Pk
	Channels      int    `orm:"column(channels)"`       //通道数
	Owner         string `orm:"column(owner)"`          //profileId/appId
	Category      int    `orm:"column(category)"`       //类型 profile:1/app:2
	DecodeHandler string `orm:"column(decode_handler)"` //解码器
	Describe      string `orm:"column(describe)"`       //描述
	app.TableInfo
}
